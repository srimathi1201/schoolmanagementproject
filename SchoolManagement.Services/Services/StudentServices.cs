﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolManagement.Business;
using SchoolManagement.ViewModel;

namespace SchoolManagement.Services
{
    public class StudentServices
    {
        public List<StudentDetailVM> GetStudentList()
        {
            try
            {
                List<StudentDetailVM> _objStudentVMList = new List<StudentDetailVM>();
                StudentDetailBL _objStudentBL = new StudentDetailBL();

                _objStudentVMList = _objStudentBL.GetStudentList();

                return _objStudentVMList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public StudentDetailVM GetStudentById(int Id)
        {
            try
            {
                StudentDetailVM _objStudentVM = new StudentDetailVM();
                StudentDetailBL _objStudentBL = new StudentDetailBL();

                _objStudentVM = _objStudentBL.GetStudentById(Id);

                return _objStudentVM;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public StudentDetailVM Create(StudentDetailVM _objStudentVM)
        {
            try
            {

                StudentDetailBL _objStudentBL = new StudentDetailBL();

                return _objStudentBL.create(_objStudentVM);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public StudentDetailVM Update(StudentDetailVM _objStudentVM)
        {
            try
            {

                StudentDetailBL _objStudentBL = new StudentDetailBL();

                return _objStudentBL.update(_objStudentVM);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool Delete(int _id)
        {
            try
            {
                StudentDetailVM _objStudentVM = new StudentDetailVM();
                StudentDetailBL _objStudentBL = new StudentDetailBL();

                return _objStudentBL.delete(_id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
