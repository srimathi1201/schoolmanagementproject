﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolManagement.ViewModel;
using SchoolManagement.Business;

namespace SchoolManagement.Services
{
    public class TeacherServices
    {
        public List<TeacherVM> GetTeacherList()
        {
            try
            {
                List<TeacherVM> _objTeacherVMList = new List<TeacherVM>();
                TeacherBL _objTeacherBL = new TeacherBL();

                _objTeacherVMList = _objTeacherBL.GetTeacherList();

                return _objTeacherVMList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public TeacherVM GetTeacherById(int Id)
        {
            try
            {
                TeacherVM _objTeacherVM = new TeacherVM();
                TeacherBL _objTeacherBL = new TeacherBL();

                _objTeacherVM = _objTeacherBL.GetTeacherById(Id);

                return _objTeacherVM;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public TeacherVM Create(TeacherVM _objTeacherVM)
        {
            try
            {

                TeacherBL _objTeacherBL = new TeacherBL();

                return _objTeacherBL.create(_objTeacherVM);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public TeacherVM Update(TeacherVM _objTeacherVM)
        {
            try
            {

                TeacherBL _objTeacherBL = new TeacherBL();

                return _objTeacherBL.update(_objTeacherVM);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool Delete(int _id)
        {
            try
            {
                TeacherVM _objTeacherVM = new TeacherVM();
                TeacherBL _objTeacherBL = new TeacherBL();

                return _objTeacherBL.delete(_id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
