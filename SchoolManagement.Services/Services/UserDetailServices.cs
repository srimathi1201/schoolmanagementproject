﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolManagement.Business;
using SchoolManagement.ViewModel;

namespace SchoolManagement.Services
{
    public class UserDetailServices
    {
        public bool Login(UserDetailVM _objUserDetailVM)
        {
            try
            {

                UserDetailBL _objUserBL = new UserDetailBL();

                return _objUserBL.Login(_objUserDetailVM);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<UserDetailVM> GetUserList()
        {
            try
            {
                List<UserDetailVM> _objUserVMList = new List<UserDetailVM>();
                UserDetailBL _objUserBL = new UserDetailBL();

                _objUserVMList = _objUserBL.GetUserList();

                return _objUserVMList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public UserDetailVM GetUserById(int Id)
        {
            try
            {
                UserDetailVM _objUserVM = new UserDetailVM();
                UserDetailBL _objUserBL = new UserDetailBL();

                _objUserVM = _objUserBL.GetUserById(Id);

                return _objUserVM;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public UserDetailVM Create(UserDetailVM _objUserVM)
        {
            try
            {

                UserDetailBL _objUserBL = new UserDetailBL();

                return _objUserBL.create(_objUserVM);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public UserDetailVM Update(UserDetailVM _objUserVM)
        {
            try
            {

                UserDetailBL _objUserBL = new UserDetailBL();

                return _objUserBL.update(_objUserVM);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool Delete(int _id)
        {
            try
            {
                UserDetailVM _objUserVM = new UserDetailVM();
                UserDetailBL _objUserBL = new UserDetailBL();

                return _objUserBL.delete(_id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
