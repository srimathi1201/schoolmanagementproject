﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolManagement.ViewModel;
using SchoolManagement.Business;

namespace SchoolManagement.Services
{
    public class MarkServices
    {
        public List<MarkVM> GetMarkList()
        {
            try
            {
                List<MarkVM> _objMarkVMList = new List<MarkVM>();
                MarkBL _objMarkBL = new MarkBL();

                _objMarkVMList = _objMarkBL.GetMarkList();

                return _objMarkVMList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public MarkVM GetMarkById(int Id)
        {
            try
            {
                MarkVM _objMarkVM = new MarkVM();
                MarkBL _objMarkBL = new MarkBL();

                _objMarkVM = _objMarkBL.GetMarkById(Id);

                return _objMarkVM;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public MarkVM Create(MarkVM _objMarkVM)
        {
            try
            {

                MarkBL _objMarkBL = new MarkBL();

                return _objMarkBL.create(_objMarkVM);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public MarkVM Update(MarkVM _objMarkVM)
        {
            try
            {

                MarkBL _objMarkBL = new MarkBL();

                return _objMarkBL.update(_objMarkVM);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool Delete(int _id)
        {
            try
            {
                MarkBL _objMarkBL = new MarkBL();

                return _objMarkBL.delete(_id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public List<MarkVM> GetByStudentId(MarkVM _objMarkVM)
        {
            try
            {
                MarkBL _objMarkBL = new MarkBL();

                return _objMarkBL.GetByStudentId(_objMarkVM);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
