﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolManagement.Models;
using SchoolManagement.ViewModel;
using SchoolManagement.Data.Repository;

namespace SchoolManagement.Business
{
    public class MarkBL : GenericBL<Mark>
    {
        public List<MarkVM> GetMarkList()
        {
            try
            {
                List<MarkVM> _objMarkVMList = new List<MarkVM>();
                List<Mark> _objMarkList = new List<Mark>();
                MarkVM _objMarkVM = new MarkVM();
                MarkBL _objMarkBL = new MarkBL();

                _objMarkList = _objMarkBL.SelectAll();

                _objMarkVMList = _objMarkVM.ToViewModelList(_objMarkList);
                return _objMarkVMList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public MarkVM GetMarkById(int Id)
        {
            try
            {
                MarkVM _objMarkVM = new MarkVM();
                Mark _objMark = new Mark();
                MarkBL _objMarkBL = new MarkBL();

                _objMark = _objMarkBL.SelectById(Id);

                _objMarkVM = _objMarkVM.ToViewModel(_objMark);

                return _objMarkVM;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public MarkVM create(MarkVM _objMarkVM)
        {

            MarkBL _objMarkBL = new MarkBL();
            Mark _objMark = new Mark();

            MarkRepository _objMarkRepo = new MarkRepository();
            _objMarkVM = _objMarkRepo.Create(_objMarkVM);

            //_objMark = _objMarkVM.ToModel(_objMarkVM);

            //_objMark = _objMarkBL.Add(_objMark);

            //_objMarkVM = _objMarkVM.ToViewModel(_objMark);

            return _objMarkVM;
        }

        public MarkVM update(MarkVM _objMarkVM)
        {

            MarkBL _objMarkBL = new MarkBL();
            Mark _objMark = new Mark();

            MarkRepository _objMarkRepo = new MarkRepository();
            _objMarkVM = _objMarkRepo.Update(_objMarkVM);

            //_objMark = _objMarkVM.ToModel(_objMarkVM);

            //_objMark = _objMarkBL.Update(_objMark);

            //_objMarkVM = _objMarkVM.ToViewModel(_objMark);


            return _objMarkVM;
        }

        public bool delete(int _id)
        {
            MarkBL _objMarkBL = new MarkBL();
            _objMarkBL.Delete(_id);

            return true;
        }
        public List<MarkVM> GetByStudentId(MarkVM _objMarkVM)
        {
            
            MarkRepository _objMarkRepo = new MarkRepository();
            return _objMarkRepo.GetByStudentId(_objMarkVM);
          
        }
    }
}
