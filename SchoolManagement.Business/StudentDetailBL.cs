﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolManagement.Data;
using SchoolManagement.ViewModel;
using SchoolManagement.Models;
using SchoolManagement.Data.Repository;

namespace SchoolManagement.Business
{
    public class StudentDetailBL : GenericBL<StudentDetail>
    {
        public List<StudentDetailVM> GetStudentList()
        {
            try
            {
                List<StudentDetailVM> _objStudentVMList = new List<StudentDetailVM>();
                List<StudentDetail> _objStudentList = new List<StudentDetail>();
                StudentDetailVM _objStudentVM = new StudentDetailVM();
                StudentDetailBL _objStudentBL = new StudentDetailBL();

                _objStudentList = _objStudentBL.SelectAll();

                _objStudentVMList = _objStudentVM.ToViewModelList(_objStudentList);
                return _objStudentVMList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public StudentDetailVM GetStudentById(int Id)
        {
            try
            {
                StudentDetailVM _objStudentVM = new StudentDetailVM();
                StudentDetail _objStudent = new StudentDetail();
                StudentDetailBL _objStudentBL = new StudentDetailBL();

                _objStudent = _objStudentBL.SelectById(Id);

                _objStudentVM = _objStudentVM.ToViewModel(_objStudent);

                return _objStudentVM;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public StudentDetailVM create(StudentDetailVM _objStudentVM)
        {

            StudentDetailBL _objStudentBL = new StudentDetailBL();
            StudentDetail _objStudent = new StudentDetail();

            StudentRepository _objStudRepo = new StudentRepository();
            _objStudentVM = _objStudRepo.Create(_objStudentVM);

            //_objStudent = _objStudentVM.ToModel(_objStudentVM);

            //_objStudent = _objStudentBL.Add(_objStudent);

            //_objStudentVM = _objStudentVM.ToViewModel(_objStudent);

            return _objStudentVM;
        }

        public StudentDetailVM update(StudentDetailVM _objStudentVM)
        {

            StudentDetailBL _objStudentBL = new StudentDetailBL();
            StudentDetail _objStudent = new StudentDetail();
            StudentRepository _objStudRepo = new StudentRepository();
            _objStudentVM = _objStudRepo.Update(_objStudentVM);

            //_objStudent = _objStudentVM.ToModel(_objStudentVM);

            //_objStudent = _objStudentBL.Update(_objStudent);

            //_objStudentVM = _objStudentVM.ToViewModel(_objStudent);


            return _objStudentVM;
        }

        public bool delete(int _id)
        {
            StudentDetailBL _objStudentBL = new StudentDetailBL();
            _objStudentBL.Delete(_id);

            return true;
        }
    }
}
