﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolManagement.Models;
using SchoolManagement.ViewModel;
using SchoolManagement.Data.Repository;

namespace SchoolManagement.Business
{
    public class UserDetailBL : GenericBL<UserDetail>
    {
        public bool Login(UserDetailVM _objUserDetailVM)
        {
            UserDetailRepository _objUserRepo = new UserDetailRepository();

            return _objUserRepo.Login(_objUserDetailVM);
        }
        public List<UserDetailVM> GetUserList()
        {
            try
            {
                List<UserDetailVM> _objUserVMList = new List<UserDetailVM>();
                List<UserDetail> _objUserList = new List<UserDetail>();
                UserDetailVM _objUserVM = new UserDetailVM();
                UserDetailBL _objUserBL = new UserDetailBL();

                _objUserList = _objUserBL.SelectAll();

                _objUserVMList = _objUserVM.ToViewModelList(_objUserList);
                return _objUserVMList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public UserDetailVM GetUserById(int Id)
        {
            try
            {
                UserDetailVM _objUserVM = new UserDetailVM();
                UserDetail _objUser = new UserDetail();
                UserDetailBL _objUserBL = new UserDetailBL();

                _objUser = _objUserBL.SelectById(Id);

                _objUserVM = _objUserVM.ToViewModel(_objUser);

                return _objUserVM;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public UserDetailVM create(UserDetailVM _objUserVM)
        {

            UserDetailBL _objUserBL = new UserDetailBL();
            UserDetail _objUser = new UserDetail();
            UserDetailRepository _objUserRepo = new UserDetailRepository();
            _objUserVM = _objUserRepo.Create(_objUserVM);

            //_objUser = _objUserVM.ToModel(_objUserVM);

            //_objUser = _objUserBL.Add(_objUser);

            //_objUserVM = _objUserVM.ToViewModel(_objUser);

            return _objUserVM;
        }

        public UserDetailVM update(UserDetailVM _objUserVM)
        {

            UserDetailBL _objUserBL = new UserDetailBL();
            UserDetail _objUser = new UserDetail();

            UserDetailRepository _objUserRepo = new UserDetailRepository();
            _objUserVM = _objUserRepo.Update(_objUserVM);

            //_objUser = _objUserVM.ToModel(_objUserVM);

            //_objUser = _objUserBL.Update(_objUser);

            //_objUserVM = _objUserVM.ToViewModel(_objUser);


            return _objUserVM;
        }

        public bool delete(int _id)
        { 
            UserDetailBL _objUserBL = new UserDetailBL();
            _objUserBL.Delete(_id);

            return true;
        }
    }
}
