﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolManagement.Models;
using SchoolManagement.ViewModel;
using SchoolManagement.Data.Repository;

namespace SchoolManagement.Business
{
    public class TeacherBL : GenericBL<Teacher>
    {
        public List<TeacherVM> GetTeacherList()
        {
            try
            {
                List<TeacherVM> _objTeacherVMList = new List<TeacherVM>();
                List<Teacher> _objTeacherList = new List<Teacher>();
                TeacherVM _objTeacherVM = new TeacherVM();
                TeacherBL _objTeacherBL = new TeacherBL();

                _objTeacherList = _objTeacherBL.SelectAll();

                _objTeacherVMList = _objTeacherVM.ToViewModelList(_objTeacherList);
                return _objTeacherVMList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public TeacherVM GetTeacherById(int Id)
        {
            try
            {
                TeacherVM _objTeacherVM = new TeacherVM();
                Teacher _objTeacher = new Teacher();
                TeacherBL _objTeacherBL = new TeacherBL();

                _objTeacher = _objTeacherBL.SelectById(Id);

                _objTeacherVM = _objTeacherVM.ToViewModel(_objTeacher);

                return _objTeacherVM;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public TeacherVM create(TeacherVM _objTeacherVM)
        {

            TeacherBL _objTeacherBL = new TeacherBL();
            Teacher _objTeacher = new Teacher();

            TeacherRepository _objTeacherRepo = new TeacherRepository();
            _objTeacherVM = _objTeacherRepo.Create(_objTeacherVM);

            //_objTeacher = _objTeacherVM.ToModel(_objTeacherVM);

            //_objTeacher = _objTeacherBL.Add(_objTeacher);

            //_objTeacherVM = _objTeacherVM.ToViewModel(_objTeacher);

            return _objTeacherVM;
        }

        public TeacherVM update(TeacherVM _objTeacherVM)
        {

            TeacherBL _objTeacherBL = new TeacherBL();
            Teacher _objTeacher = new Teacher();

            TeacherRepository _objTeacherRepo = new TeacherRepository();
            _objTeacherVM = _objTeacherRepo.Update(_objTeacherVM);

            //_objTeacher = _objTeacherVM.ToModel(_objTeacherVM);

            //_objTeacher = _objTeacherBL.Update(_objTeacher);

            //_objTeacherVM = _objTeacherVM.ToViewModel(_objTeacher);


            return _objTeacherVM;
        }

        public bool delete(int _id)
        {
            TeacherBL _objTeacherBL = new TeacherBL();
            _objTeacherBL.Delete(_id);

            return true;
        }
    }
}
