﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolManagement.Data.Repository;
using SchoolManagement.Data;

namespace SchoolManagement.Business
{
    public class GenericBL<T> where T : class
    {
        public GenericRepository<T> _objRepo;
        public SchoolMgmtDBContext DBContext;

        public GenericBL()
        {
            DBContext = new SchoolMgmtDBContext();
            _objRepo = new GenericRepository<T>(DBContext);
        }
        public GenericBL(SchoolMgmtDBContext _DBContext)
        {
            DBContext = _DBContext;
            _objRepo = new GenericRepository<T>(_DBContext);
        }

       public List<T> SelectAll()
        {
            return _objRepo.All();
        }

        public T SelectById(object Id)
        {
            return _objRepo.SelectById(Id);
        } 
        public T Add(T List)
        {
            T obj = _objRepo.Add(List);
            _objRepo.Save();

            return obj;
        }

        public void InsertAll(IEnumerable<T> List)
        {

            foreach (var item in List)
            {
                _objRepo.Add(item);
                _objRepo.Save();
            }

        }
        public T Update(T _obj)
        {

            T obj = _objRepo.Update(_obj);
            _objRepo.Save();
            return obj;
        }

        public void Delete(object _obj)
        {
            _objRepo.Delete(_obj);
            _objRepo.Save();
        }


        
    }
}
