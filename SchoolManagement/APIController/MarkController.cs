﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SchoolManagement.ViewModel;
using SchoolManagement.Services;

namespace SchoolManagement.APIController
{
    [RoutePrefix("api/Mark")]
    public class MarkController : ApiController
    { 
        [HttpGet]
        public IHttpActionResult GetMarks()
        {
            MarkServices _objMarkServices = new MarkServices();
            MarkVM _objMarkVM = new MarkVM();
            _objMarkVM.MarkList = new List<MarkVM>();

            _objMarkVM.MarkList = _objMarkServices.GetMarkList();

            return Ok(_objMarkVM.MarkList);
        }

        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult GetMarkById(int id)
        {
            MarkServices _objMarkServices = new MarkServices();
            MarkVM _objMarkVM = new MarkVM();

            _objMarkVM = _objMarkServices.GetMarkById(id);


            return Ok(_objMarkVM);
        }

        [HttpPost]
        [Route("create")]
        public IHttpActionResult Create(MarkVM _objMarkVM)
        {
            try
            {
                MarkServices _objMarkServices = new MarkServices();
                _objMarkServices.Create(_objMarkVM);
                return Ok(_objMarkVM);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPut]
        [Route("update")]
        public IHttpActionResult Update(MarkVM _objMarkVM)
        {
            try
            {

                MarkServices _objMarkServices = new MarkServices();
                _objMarkServices.Update(_objMarkVM);
                return Ok(_objMarkVM);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpDelete]
        [Route("{id}")]
        public IHttpActionResult Delete(int Id)
        {
            try
            {
                MarkServices _objMarkServices = new MarkServices();
                var result = _objMarkServices.Delete(Id);
                return Ok(result);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
