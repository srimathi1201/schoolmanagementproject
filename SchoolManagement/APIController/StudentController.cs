﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SchoolManagement.Services;
using SchoolManagement.ViewModel;

namespace SchoolManagement.APIController
{
    [RoutePrefix("api/Student")]
    public class StudentController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetStudents()
        {
            StudentServices _objStudentServices = new StudentServices();
            StudentDetailVM _objStudentVM = new StudentDetailVM();
            _objStudentVM.StudentList = new List<StudentDetailVM>();

            _objStudentVM.StudentList = _objStudentServices.GetStudentList();

            return Ok(_objStudentVM.StudentList);
        }

        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult GetStudentById(int id)
        {
            StudentServices _objStudentServices = new StudentServices();
            StudentDetailVM _objStudentVM = new StudentDetailVM();

            _objStudentVM = _objStudentServices.GetStudentById(id);


            return Ok(_objStudentVM);
        }

        [HttpPost]
        [Route("create")]
        public IHttpActionResult Create(StudentDetailVM _objStudentVM)
        {
            try
            {
                StudentServices _objStudentServices = new StudentServices();
                _objStudentServices.Create(_objStudentVM);
                return Ok(_objStudentVM);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPut]
        [Route("update")]
        public IHttpActionResult Update(StudentDetailVM _objStudentVM)
        {
            try
            {

                StudentServices _objStudentServices = new StudentServices();
                _objStudentServices.Update(_objStudentVM);
                return Ok(_objStudentVM);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpDelete]
        [Route("{id}")]
        public IHttpActionResult Delete(int Id)
        {
            try
            {
                StudentServices _objStudentServices = new StudentServices();
                var result = _objStudentServices.Delete(Id);
                return Ok(result);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
