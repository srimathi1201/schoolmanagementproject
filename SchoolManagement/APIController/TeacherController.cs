﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SchoolManagement.Services;
using SchoolManagement.ViewModel;

namespace SchoolManagement.APIController
{
    [RoutePrefix("api/Teacher")]
    public class TeacherController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetTeachers()
        {
            TeacherServices _objTeacherServices = new TeacherServices();
            TeacherVM _objTeacherVM = new TeacherVM();
            _objTeacherVM.TeacherList = new List<TeacherVM>();

            _objTeacherVM.TeacherList = _objTeacherServices.GetTeacherList();

            return Ok(_objTeacherVM.TeacherList);
        }

        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult GetTeacherById(int id)
        {
            TeacherServices _objTeacherServices = new TeacherServices();
            TeacherVM _objTeacherVM = new TeacherVM();

            _objTeacherVM = _objTeacherServices.GetTeacherById(id);


            return Ok(_objTeacherVM);
        }

        [HttpPost]
        [Route("create")]
        public IHttpActionResult Create(TeacherVM _objTeacherVM)
        {
            try
            {
                TeacherServices _objTeacherServices = new TeacherServices();
                _objTeacherServices.Create(_objTeacherVM);
                return Ok(_objTeacherVM);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPut]
        [Route("update")]
        public IHttpActionResult Update(TeacherVM _objTeacherVM)
        {
            try
            {

                TeacherServices _objTeacherServices = new TeacherServices();
                _objTeacherServices.Update(_objTeacherVM);
                return Ok(_objTeacherVM);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpDelete]
        [Route("{id}")]
        public IHttpActionResult Delete(int Id)
        {
            try
            {
                TeacherServices _objTeacherServices = new TeacherServices();
                var result = _objTeacherServices.Delete(Id);
                return Ok(result);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
