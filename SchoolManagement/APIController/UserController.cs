﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SchoolManagement.ViewModel;
using SchoolManagement.Services;

namespace SchoolManagement.APIController
{
    [RoutePrefix("api/User")]
    public class UserController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetUsers()
        {
            UserDetailServices _objUserServices = new UserDetailServices();
            UserDetailVM _objUserVM = new UserDetailVM();
            _objUserVM.UserList = new List<UserDetailVM>();

            _objUserVM.UserList = _objUserServices.GetUserList();

            return Ok(_objUserVM.UserList);
        }

        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult GetEmployeeById(int id)
        {
            UserDetailServices _objUserServices = new UserDetailServices();
            UserDetailVM _objUserVM = new UserDetailVM();

            _objUserVM = _objUserServices.GetUserById(id);


            return Ok(_objUserVM);
        }

        [HttpPost]
        [Route("create")]
        public IHttpActionResult Create(UserDetailVM _objUserVM)
        {
            try
            {
                UserDetailServices _objUserServices = new UserDetailServices();
                _objUserServices.Create(_objUserVM);
                return Ok(_objUserVM);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPut]
        [Route("update")]
        public IHttpActionResult Update(UserDetailVM _objUserVM)
        {
            try
            {

                UserDetailServices _objUserServices = new UserDetailServices();
                _objUserServices.Update(_objUserVM);
                return Ok(_objUserVM);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpDelete]
        [Route("{id}")]
        public IHttpActionResult Delete(int Id)
        {
            try
            {
                UserDetailServices _objUserServices = new UserDetailServices();
                var result = _objUserServices.Delete(Id);
                return Ok(result);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


    }
}