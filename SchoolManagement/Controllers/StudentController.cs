﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;
using SchoolManagement.ViewModel;
using SchoolManagement.Services;

namespace SchoolManagement.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            try
            {
                StudentServices _objStudentServices = new StudentServices();
                StudentDetailVM _objStudentVM = new StudentDetailVM();
                _objStudentVM.StudentList = new List<StudentDetailVM>();

                _objStudentVM.StudentList = _objStudentServices.GetStudentList();
                return View(_objStudentVM);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpGet]
        public ActionResult Create()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        public ActionResult Create(StudentDetailVM _objStudentVM)
        {
            try
            {

                StudentServices _objStudentServices = new StudentServices();
                string ImgName = Path.GetFileName(_objStudentVM.Image.FileName);
                string Imgext = Path.GetExtension(ImgName);
                if(Imgext ==".jpg" || Imgext ==".png")
                {
                    string SavePath = Path.Combine(Server.MapPath("\\Content\\Images\\"), ImgName);
                    _objStudentVM.ImagePath = Path.Combine(("\\Content\\Images\\"), ImgName);
                    _objStudentVM.Image.SaveAs(SavePath);
                }
                _objStudentServices.Create(_objStudentVM);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ActionResult Edit(int Id)
        {
            try
            {
                StudentDetailVM _objStudentVM = new StudentDetailVM();
                _objStudentVM.StudentList = new List<StudentDetailVM>();

                StudentServices _objStudentServices = new StudentServices();

                _objStudentVM = _objStudentServices.GetStudentById(Id);

                return View(_objStudentVM);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        public ActionResult Edit(StudentDetailVM _objStudentVM)
        {
            try
            {

                StudentServices _objStudentServices = new StudentServices();
                _objStudentServices.Update(_objStudentVM);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public JsonResult Delete(int Id)
        {
            try
            {

                StudentServices _objStudentServices = new StudentServices();
                var result = _objStudentServices.Delete(Id);
                return Json(result);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}