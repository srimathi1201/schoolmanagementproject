﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchoolManagement.Services;
using SchoolManagement.ViewModel;

namespace SchoolManagement.Controllers
{
    public class TeacherController : Controller
    {
        // GET: Teacher
        public ActionResult Index()
        {
            try
            {
                TeacherServices _objTeacherServices = new TeacherServices();
                TeacherVM _objTeacherVM = new TeacherVM();
                _objTeacherVM.TeacherList = new List<TeacherVM>();

                _objTeacherVM.TeacherList = _objTeacherServices.GetTeacherList();
                return View(_objTeacherVM);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpGet]
        public ActionResult Create()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        public ActionResult Create(TeacherVM _objTeacherVM)
        {
            try
            {

                TeacherServices _objTeacherServices = new TeacherServices();
                _objTeacherServices.Create(_objTeacherVM);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ActionResult Edit(int Id)
        {
            try
            {
                TeacherVM _objTeacherVM = new TeacherVM();
                _objTeacherVM.TeacherList = new List<TeacherVM>();

                TeacherServices _objTeacherServices = new TeacherServices();

                _objTeacherVM = _objTeacherServices.GetTeacherById(Id);

                return View(_objTeacherVM);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        public ActionResult Edit(TeacherVM _objTeacherVM)
        {
            try
            {

                TeacherServices _objTeacherServices = new TeacherServices();
                _objTeacherServices.Update(_objTeacherVM);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public JsonResult Delete(int Id)
        {
            try
            {

                TeacherServices _objTeacherServices = new TeacherServices();
                var result = _objTeacherServices.Delete(Id);
                return Json(result);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}