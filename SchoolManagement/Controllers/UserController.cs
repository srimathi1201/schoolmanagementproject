﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchoolManagement.ViewModel;
using SchoolManagement.Services;

namespace SchoolManagement.Controllers
{
    public class UserController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(UserDetailVM _objUserVM)
        {
            try
            {
                UserDetailServices _objUserServices = new UserDetailServices();
                if (_objUserServices.Login(_objUserVM) == true)
                {
                    return RedirectToAction("GetUserList");
                }
                else
                { 
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public ActionResult GetUserList()
        {
            try
            {
                UserDetailServices _objUserServices = new UserDetailServices();
                UserDetailVM _objUserVM = new UserDetailVM();
                _objUserVM.UserList = new List<UserDetailVM>();

                _objUserVM.UserList = _objUserServices.GetUserList();
                return View(_objUserVM);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpGet]
        public ActionResult Create()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        public ActionResult Create(UserDetailVM _objUserVM)
        {
            try
            {

                UserDetailServices _objUserServices = new UserDetailServices();
                _objUserServices.Create(_objUserVM);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ActionResult Edit(int Id)
        {
            try
            {
                UserDetailVM _objUserVM = new UserDetailVM();
                _objUserVM.UserList = new List<UserDetailVM>();

                UserDetailServices _objUserServices = new UserDetailServices();

                _objUserVM = _objUserServices.GetUserById(Id);

                return View(_objUserVM);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        public ActionResult Edit(UserDetailVM _objUserVM)
        {
            try
            {

                UserDetailServices _objUserServices = new UserDetailServices();
                _objUserServices.Update(_objUserVM);

                return RedirectToAction("GetUserList");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public JsonResult Delete(int Id)
        {
            try
            {

                UserDetailServices _objUserServices = new UserDetailServices();
                var result = _objUserServices.Delete(Id);
                return Json(result);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}