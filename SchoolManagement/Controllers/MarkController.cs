﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchoolManagement.ViewModel;
using SchoolManagement.Services;

namespace SchoolManagement.Controllers
{
    public class MarkController : Controller
    {   
        public ActionResult Index()
        {
            try
            {
                MarkServices _objMarkServices = new MarkServices();
                MarkVM _objMarkVM = new MarkVM();
                _objMarkVM.MarkList = new List<MarkVM>();

                _objMarkVM.MarkList = _objMarkServices.GetMarkList();
                return View(_objMarkVM);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpGet]
        public ActionResult Create()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        public ActionResult Create(MarkVM _objMarkVM)
        {
            try
            {

                MarkServices _objMarkServices = new MarkServices();
                _objMarkServices.Create(_objMarkVM);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ActionResult Edit(int Id)
        {
            try
            {
                MarkVM _objMarkVM = new MarkVM();
                _objMarkVM.MarkList = new List<MarkVM>();

                MarkServices _objMarkServices = new MarkServices();

                _objMarkVM = _objMarkServices.GetMarkById(Id);

                return View(_objMarkVM);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        public ActionResult Edit(MarkVM _objMarkVM)
        {
            try
            {

                MarkServices _objMarkServices = new MarkServices();
                _objMarkServices.Update(_objMarkVM);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public JsonResult Delete(int Id)
        {
            try
            {

                MarkServices _objMarkServices = new MarkServices();
                var result = _objMarkServices.Delete(Id);
                return Json(result);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public ActionResult GetByStudentId(MarkVM _objMarkVM)
        {
            try
            {
                List<MarkVM> _objMarkVMList = new List<MarkVM>();
                MarkServices _objMarkServices = new MarkServices();
                _objMarkVMList = _objMarkServices.GetByStudentId(_objMarkVM);

                return View("Index", _objMarkVMList);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}