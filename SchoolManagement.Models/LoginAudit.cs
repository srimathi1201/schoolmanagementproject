﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolManagement.Models
{
    public class LoginAudit
    {
        public int ID { get; set; }
        public int UserId { get; set; }
        public string LoginStatus { get; set; }
        public DateTime LoginDate { get; set; }
    }
}
