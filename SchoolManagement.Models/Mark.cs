﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolManagement.Models
{
    public class Mark
    {
        public int ID { get; set; }
        public int Dept_Id { get; set; }
        public int Subject_Id { get; set; }
        public int Marks { get; set; }
        public int Student_id { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

    }
}
