﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using SchoolManagement.Models;

namespace SchoolManagement.Data
{
    public class SchoolMgmtDBContext : DbContext
    {
        public SchoolMgmtDBContext() : base("name=SchoolMgmtConnection")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

        }

        public virtual DbSet<UserDetail> UserDetails { get; set; }
        public virtual DbSet<StudentDetail> StudentDetails { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Subject> Subjects { get; set; }
        public virtual DbSet<Teacher> Teachers { get; set; }
        public virtual DbSet<Mark> Marks { get; set; }
        public virtual DbSet<LoginAudit> LoginAudits { get; set; }
    }
}
