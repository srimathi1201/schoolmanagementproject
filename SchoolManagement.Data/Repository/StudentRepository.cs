﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolManagement.ViewModel;
using SchoolManagement.Models;
using System.Data.SqlClient;
using System.Web;

namespace SchoolManagement.Data.Repository
{
    public class StudentRepository
    {
        public StudentDetailVM Create(StudentDetailVM _objStudDetailVM)
        {
            try
            {
                using (SchoolMgmtDBContext SchlDBContext = new SchoolMgmtDBContext())
                {
                    SchlDBContext.Database.SqlQuery<StudentDetailVM>
                        ("exec usp_Students @Id, @Name,@DOB,@Image,@Gender,@Address,@FatherName,@MobileNo,@Email,@DeptId, @CreatedBy,@UpdatedBy, @flag",
                                                                              new SqlParameter("@Id", _objStudDetailVM.ID),
                                                                              new SqlParameter("@Name", _objStudDetailVM.Name),
                                                                              new SqlParameter("@DOB", _objStudDetailVM.DOB),
                                                                              new SqlParameter("@Image", _objStudDetailVM.ImagePath),
                                                                              new SqlParameter("@Gender", _objStudDetailVM.Gender),
                                                                              new SqlParameter("@Address", _objStudDetailVM.Address),
                                                                              new SqlParameter("@FatherName", _objStudDetailVM.FatherName),
                                                                              new SqlParameter("@Email", _objStudDetailVM.Email),
                                                                              new SqlParameter("@DeptId", _objStudDetailVM.Dept_Id),
                                                                              new SqlParameter("@MobileNo", _objStudDetailVM.MobileNo),
                                                                             
                                                                              new SqlParameter("@CreatedBy", _objStudDetailVM.CreatedBy),
                                                                              new SqlParameter("@UpdatedBy", _objStudDetailVM.UpdatedBy),
                                                                              new SqlParameter("@flag", "Insert")
                                                                              ).SingleOrDefault();

                }
                return _objStudDetailVM;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public StudentDetailVM Update(StudentDetailVM _objStudDetailVM)
        {
            try
            {
                using (SchoolMgmtDBContext SchlDBContext = new SchoolMgmtDBContext())
                {
                    SchlDBContext.Database.SqlQuery<StudentDetailVM>
                        ("exec usp_Students @Id, @Name,@DOB,@Image,@Gender,@Address,@FatherName,@MobileNo,@Email,@DeptId, @CreatedBy,@UpdatedBy, @flag",
                                                                               new SqlParameter("@Id", _objStudDetailVM.ID),
                                                                              new SqlParameter("@Name", _objStudDetailVM.Name),
                                                                              new SqlParameter("@DOB", _objStudDetailVM.DOB),
                                                                              new SqlParameter("@Image", _objStudDetailVM.Image),
                                                                              new SqlParameter("@Gender", _objStudDetailVM.Gender),
                                                                              new SqlParameter("@Address", _objStudDetailVM.Address),
                                                                              new SqlParameter("@FatherName", _objStudDetailVM.FatherName),
                                                                              new SqlParameter("@Email", _objStudDetailVM.Email),
                                                                              new SqlParameter("@DeptId", _objStudDetailVM.Dept_Id),
                                                                              new SqlParameter("@MobileNo", _objStudDetailVM.MobileNo),
                                                                       
                                                                              new SqlParameter("@CreatedBy", _objStudDetailVM.CreatedBy),
                                                                              new SqlParameter("@UpdatedBy", _objStudDetailVM.UpdatedBy),
                                                                              new SqlParameter("@flag", "Update")
                                                                              ).SingleOrDefault();

                }
                return _objStudDetailVM;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
