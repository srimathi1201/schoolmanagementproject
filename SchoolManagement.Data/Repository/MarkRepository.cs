﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolManagement.ViewModel;
using SchoolManagement.Models;
using System.Data.SqlClient;

namespace SchoolManagement.Data.Repository
{
    public class MarkRepository
    {
        public MarkVM Create(MarkVM _objMarkVM)
        {
            try
            {
                using (SchoolMgmtDBContext SchlDBContext = new SchoolMgmtDBContext())
                {
                    SchlDBContext.Database.SqlQuery<MarkVM>
                         ("exec usp_Marks @Id, @DeptId,@SubjectId, @StudentId,@Marks, @CreatedBy,@UpdatedBy, @flag",
                                                                               new SqlParameter("@Id", _objMarkVM.ID),
                                                                              new SqlParameter("@DeptId", _objMarkVM.Dept_Id),
                                                                              new SqlParameter("@SubjectId", _objMarkVM.Subject_Id),
                                                                             new SqlParameter("@StudentId", _objMarkVM.Student_id),
                                                                              new SqlParameter("@Marks", _objMarkVM.Marks),
                                                                              new SqlParameter("@CreatedBy", _objMarkVM.CreatedBy),
                                                                              new SqlParameter("@UpdatedBy", _objMarkVM.UpdatedBy),
                                                                              new SqlParameter("@flag", "Insert")
                                                                              ).SingleOrDefault();

                }
                return _objMarkVM;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public MarkVM Update(MarkVM _objMarkVM)
        {
            try
            {
                using (SchoolMgmtDBContext SchlDBContext = new SchoolMgmtDBContext())
                {
                    SchlDBContext.Database.SqlQuery<MarkVM>
                        ("exec usp_Marks @Id, @DeptId,@SubjectId, @StudentId,@Marks, @CreatedBy,@UpdatedBy, @flag",
                                                                               new SqlParameter("@Id", _objMarkVM.ID),
                                                                              new SqlParameter("@DeptId", _objMarkVM.Dept_Id),
                                                                              new SqlParameter("@SubjectId", _objMarkVM.Subject_Id),
                                                                             new SqlParameter("@StudentId", _objMarkVM.Student_id),
                                                                              new SqlParameter("@Marks", _objMarkVM.Marks),
                                                                           
                                                                              new SqlParameter("@CreatedBy", _objMarkVM.CreatedBy),
                                                                              new SqlParameter("@UpdatedBy", _objMarkVM.UpdatedBy),
                                                                              new SqlParameter("@flag", "Update")
                                                                              ).SingleOrDefault();

                }
                return _objMarkVM;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<MarkVM> GetByStudentId(MarkVM _objMarkVM)
        {
            try
            {
                using (SchoolMgmtDBContext SchlDBContext = new SchoolMgmtDBContext())
                {
                    List<MarkVM> _objMarkVMList = new List<MarkVM>();
                    _objMarkVMList = SchlDBContext.Marks.Where(a => a.Student_id == _objMarkVM.Student_id).Select(y => new MarkVM()
                    {
                        ID = y.ID,
                        Dept_Id = y.Dept_Id,
                        Subject_Id = y.Subject_Id,
                        Marks = y.Marks,
                        Student_id = y.Student_id,
                        CreatedBy = y.CreatedBy,
                        UpdatedBy = y.UpdatedBy,
                        CreatedDate = y.CreatedDate,
                        UpdatedDate = y.UpdatedDate
                    }).ToList();

                    return _objMarkVMList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
