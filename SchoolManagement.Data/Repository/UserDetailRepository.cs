﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolManagement.Models;
using SchoolManagement.ViewModel;
using System.Data.Entity;
using System.Data.SqlClient;

namespace SchoolManagement.Data.Repository
{
    public class UserDetailRepository
    {
        public bool Login(UserDetailVM _objUserDetailVM)
        {
            try
            {
                using (SchoolMgmtDBContext SchlDBContext = new SchoolMgmtDBContext())
                {
                    var obj = SchlDBContext.UserDetails.Where(a => a.Email.Equals(_objUserDetailVM.Email) &&
                                                     a.Password.Equals(_objUserDetailVM.Password)).FirstOrDefault();
                    if (obj != null)
                    { 
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public UserDetailVM Create(UserDetailVM _objUserDetailVM)
        {
            try
            {
                using (SchoolMgmtDBContext SchlDBContext = new SchoolMgmtDBContext())
                {
                    SchlDBContext.Database.SqlQuery<UserDetailVM>
                        ("exec usp_Users @Id, @Name,@Gender,@Designation,@Address,@MobileNo,@Email,@Password,@CreatedBy,@UpdatedBy, @flag",
                                                                              new SqlParameter("@Id", _objUserDetailVM.ID),
                                                                              new SqlParameter("@Name", _objUserDetailVM.Name),
                                                                              new SqlParameter("@Gender", _objUserDetailVM.Gender),
                                                                              new SqlParameter("@Designation", _objUserDetailVM.Designation),
                                                                              new SqlParameter("@Address", _objUserDetailVM.Address),
                                                                              new SqlParameter("@MobileNo", _objUserDetailVM.MobileNo),
                                                                              new SqlParameter("@Email", _objUserDetailVM.Email),
                                                                              new SqlParameter("@Password", _objUserDetailVM.Password),
                                                                              
                                                                              new SqlParameter("@CreatedBy", _objUserDetailVM.CreatedBy),
                                                                              new SqlParameter("@UpdatedBy", _objUserDetailVM.UpdatedBy),
                                                                              new SqlParameter("@flag", "Insert")
                                                                              ).SingleOrDefault();
                }
                return _objUserDetailVM;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public UserDetailVM Update(UserDetailVM _objUserDetailVM)
        {
            try
            {
                using (SchoolMgmtDBContext SchlDBContext = new SchoolMgmtDBContext())
                {
                    SchlDBContext.Database.SqlQuery<UserDetailVM>
                        ("exec usp_Users @Id, @Name,@Gender,@Designation,@Address,@MobileNo,@Email,@Password,@CreatedBy,@UpdatedBy, @flag",
                                                                              new SqlParameter("@Id", _objUserDetailVM.ID),
                                                                              new SqlParameter("@Name", _objUserDetailVM.Name),
                                                                              new SqlParameter("@Gender", _objUserDetailVM.Gender),
                                                                              new SqlParameter("@Designation", _objUserDetailVM.Designation),
                                                                              new SqlParameter("@Address", _objUserDetailVM.Address),
                                                                              new SqlParameter("@MobileNo", _objUserDetailVM.MobileNo),
                                                                              new SqlParameter("@Email", _objUserDetailVM.Email),
                                                                              new SqlParameter("@Password", _objUserDetailVM.Password),
                                                                              new SqlParameter("@CreatedBy", _objUserDetailVM.CreatedBy),
                                                                              new SqlParameter("@UpdatedBy", _objUserDetailVM.UpdatedBy),
                                                                          
                                                                              new SqlParameter("@flag", "Update")
                                                                              ).SingleOrDefault();

                }
                return _objUserDetailVM;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
