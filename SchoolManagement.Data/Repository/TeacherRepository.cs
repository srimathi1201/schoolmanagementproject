﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolManagement.ViewModel;
using SchoolManagement.Models;
using System.Data.SqlClient;

namespace SchoolManagement.Data.Repository
{
    public class TeacherRepository
    {
        public TeacherVM Create(TeacherVM _objTeacherVM)
        {
            try
            {
                using (SchoolMgmtDBContext SchlDBContext = new SchoolMgmtDBContext())
                {
                    SchlDBContext.Database.SqlQuery<TeacherVM>
                        ("exec usp_Teachers @Id, @Name,@DOB,@Gender,@Address,@MobileNo,@Email,@SubjectId, @CreatedBy,@UpdatedBy, @flag",
                                                                              new SqlParameter("@Id", _objTeacherVM.ID),
                                                                              new SqlParameter("@Name", _objTeacherVM.Name),
                                                                              new SqlParameter("@DOB", _objTeacherVM.DOB),
                                                                              new SqlParameter("@Gender", _objTeacherVM.Gender),
                                                                              new SqlParameter("@Address", _objTeacherVM.Address),
                                                                              new SqlParameter("@Email", _objTeacherVM.Email),
                                                                              new SqlParameter("@SubjectId", _objTeacherVM.Subject_Id),
                                                                              new SqlParameter("@MobileNo", _objTeacherVM.MobileNo),
                                                                              new SqlParameter("@CreatedBy", _objTeacherVM.CreatedBy),
                                                                              new SqlParameter("@UpdatedBy", _objTeacherVM.UpdatedBy),
                                                                              new SqlParameter("@flag", "Insert")
                                                                              ).SingleOrDefault();

                }
                return _objTeacherVM;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public TeacherVM Update(TeacherVM _objTeacherVM)
        {
            try
            {
                using (SchoolMgmtDBContext SchlDBContext = new SchoolMgmtDBContext())
                {
                    SchlDBContext.Database.SqlQuery<TeacherVM>
                        ("exec usp_Teachers @Id, @Name,@DOB,@Gender,@Address,@MobileNo,@Email,@SubjectId, @CreatedBy,@UpdatedBy, @flag",
                                                                               new SqlParameter("@Id", _objTeacherVM.ID),
                                                                              new SqlParameter("@Name", _objTeacherVM.Name),
                                                                              new SqlParameter("@DOB", _objTeacherVM.DOB),
                                                                             new SqlParameter("@Gender", _objTeacherVM.Gender),
                                                                              new SqlParameter("@Address", _objTeacherVM.Address),
                                                                              new SqlParameter("@Email", _objTeacherVM.Email),
                                                                              new SqlParameter("@SubjectId", _objTeacherVM.Subject_Id),
                                                                              new SqlParameter("@MobileNo", _objTeacherVM.MobileNo),
                                                                            
                                                                              new SqlParameter("@CreatedBy", _objTeacherVM.CreatedBy),
                                                                              new SqlParameter("@UpdatedBy", _objTeacherVM.UpdatedBy),
                                                                              new SqlParameter("@flag", "Update")
                                                                              ).SingleOrDefault();

                }
                return _objTeacherVM;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
