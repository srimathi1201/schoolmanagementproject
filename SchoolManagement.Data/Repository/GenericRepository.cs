﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolManagement.Models;
using System.Data.Entity;

namespace SchoolManagement.Data.Repository
{
    public class GenericRepository<T> where T : class
    {
        public SchoolMgmtDBContext ShlDBContext = null;
        private DbSet<T> table = null;

        public GenericRepository()
        {
            this.ShlDBContext = new SchoolMgmtDBContext();
            table = ShlDBContext.Set<T>();
        }

        public GenericRepository(SchoolMgmtDBContext _objShlDBContext)
        {
            this.ShlDBContext = _objShlDBContext;
            table = _objShlDBContext.Set<T>();
        }

        public List<T> All()
        {
            return table.ToList();
        }
        
        public T SelectById(object id)
        {
            return table.Find(id);
        }

        public T Add(T obj)
        {
            try
            {
                return table.Add(obj);
            }
            catch(Exception Ex)
            {
                throw Ex;
            }
        } 
        public T Update(T obj)
        {
            try
            {
                var result = table.Attach(obj);
                ShlDBContext.Entry(obj).State = EntityState.Modified;
                return result;
            }
            catch(Exception Ex)
            {
                throw Ex;
            }
        }
        public void Delete(object id)
        {
            try
            {
                T existing = table.Find(id);
                table.Remove(existing);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save()
        {

            ShlDBContext.SaveChanges();
        }
    }
}
