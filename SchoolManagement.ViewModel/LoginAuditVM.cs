﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolManagement.ViewModel
{
    public class LoginAuditVM
    {
        public int ID { get; set; }
        public int UserId { get; set; }
        public string LoginStatus { get; set; }
        public DateTime LoginDate { get; set; }
    }
}
