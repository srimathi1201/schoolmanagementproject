﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolManagement.Models;
using System.ComponentModel.DataAnnotations;

namespace SchoolManagement.ViewModel
{
    public class TeacherVM
    {
        public int ID { get; set; }
        
        public string Name { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }

        [Display(Name = "Subject ID")]
        public int Subject_Id { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public List<TeacherVM> TeacherList { get; set; }

        public Teacher ToModel(TeacherVM _objTeacherVM)
        {
            Teacher _objTeacher = new Teacher();

            _objTeacher.ID = _objTeacherVM.ID;
            _objTeacher.Name = _objTeacherVM.Name;
            _objTeacher.DOB = _objTeacherVM.DOB;
            _objTeacher.Gender = _objTeacherVM.Gender;
            _objTeacher.Address = _objTeacherVM.Address;
            _objTeacher.Subject_Id = _objTeacherVM.Subject_Id;
            _objTeacher.MobileNo = _objTeacherVM.MobileNo;
            _objTeacher.Email = _objTeacherVM.Email;
            _objTeacher.CreatedBy = _objTeacherVM.CreatedBy;
            _objTeacher.UpdatedBy = _objTeacherVM.UpdatedBy;
            _objTeacher.CreatedDate = _objTeacherVM.CreatedDate;
            _objTeacher.UpdatedDate = _objTeacherVM.UpdatedDate;

            return _objTeacher;
        }

        public TeacherVM ToViewModel(Teacher _objTeacher)
        {
            TeacherVM _objTeacherVM = new TeacherVM();

            _objTeacherVM.ID = _objTeacher.ID;
            _objTeacherVM.Name = _objTeacher.Name;
            _objTeacherVM.DOB = _objTeacher.DOB;
            _objTeacherVM.Gender = _objTeacher.Gender;
            _objTeacherVM.Address = _objTeacher.Address;
            _objTeacherVM.Subject_Id = _objTeacher.Subject_Id;
            _objTeacherVM.MobileNo = _objTeacher.MobileNo;
            _objTeacherVM.Email = _objTeacher.Email;
            _objTeacherVM.CreatedBy = _objTeacher.CreatedBy;
            _objTeacherVM.UpdatedBy = _objTeacher.UpdatedBy;
            _objTeacherVM.CreatedDate = _objTeacher.CreatedDate;
            _objTeacherVM.UpdatedDate = _objTeacher.UpdatedDate;

            return _objTeacherVM;
        }
        public List<TeacherVM> ToViewModelList(List<Teacher> _objTeacherList)
        {

            List<TeacherVM> _objTeacherVMList = new List<TeacherVM>();

            foreach (var i in _objTeacherList)
            {

                TeacherVM _objTeacherVM = new TeacherVM();
                _objTeacherVM.ID = i.ID;
                _objTeacherVM.Name = i.Name;
                _objTeacherVM.DOB = i.DOB;
                _objTeacherVM.Gender = i.Gender;
                _objTeacherVM.Address = i.Address;
                _objTeacherVM.Subject_Id = i.Subject_Id;
                _objTeacherVM.MobileNo = i.MobileNo;
                _objTeacherVM.Email = i.Email;
                _objTeacherVM.CreatedBy = i.CreatedBy;
                _objTeacherVM.UpdatedBy = i.UpdatedBy;
                _objTeacherVM.CreatedDate = i.CreatedDate;
                _objTeacherVM.UpdatedDate = i.UpdatedDate;
                _objTeacherVMList.Add(_objTeacherVM);

            }


            return _objTeacherVMList;
        }
    }
}
