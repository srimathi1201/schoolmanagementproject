﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolManagement.Models;
using System.ComponentModel.DataAnnotations;

namespace SchoolManagement.ViewModel
{
    public class MarkVM
    {
        public int ID { get; set; }

        public int Dept_Id { get; set; }

        public int Subject_Id { get; set; }

        public int Marks { get; set; }

        public int Student_id { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public List<MarkVM> MarkList { get; set; }

        public Mark ToModel(MarkVM _objMarkVM)
        {
            Mark _objMark = new Mark();

            _objMark.ID = _objMarkVM.ID;
            _objMark.Dept_Id = _objMarkVM.Dept_Id;
            _objMark.Subject_Id = _objMarkVM.Subject_Id;
            _objMark.Marks = _objMarkVM.Marks;
            _objMark.Student_id = _objMarkVM.Student_id;
            _objMark.CreatedBy = _objMarkVM.CreatedBy;
            _objMark.UpdatedBy = _objMarkVM.UpdatedBy;
            _objMark.CreatedDate = _objMarkVM.CreatedDate;
            _objMark.UpdatedDate = _objMarkVM.UpdatedDate;

            return _objMark;
        }

        public MarkVM ToViewModel(Mark _objMark)
        {
            MarkVM _objMarkVM = new MarkVM();

            _objMarkVM.ID = _objMark.ID;
            _objMarkVM.Dept_Id = _objMark.Dept_Id;
            _objMarkVM.Subject_Id = _objMark.Subject_Id;
            _objMarkVM.Marks = _objMark.Marks;
            _objMarkVM.Student_id = _objMark.Student_id;
            _objMarkVM.CreatedBy = _objMark.CreatedBy;
            _objMarkVM.UpdatedBy = _objMark.UpdatedBy;
            _objMarkVM.CreatedDate = _objMark.CreatedDate;
            _objMarkVM.UpdatedDate = _objMark.UpdatedDate;

            return _objMarkVM;
        }
        public List<MarkVM> ToViewModelList(List<Mark> _objMarkList)
        {

            List<MarkVM> _objMarkVMList = new List<MarkVM>();

            foreach (var i in _objMarkList)
            {

                MarkVM _objMarkVM = new MarkVM();
                _objMarkVM.ID = i.ID;
                _objMarkVM.Dept_Id = i.Dept_Id;
                _objMarkVM.Subject_Id = i.Subject_Id;
                _objMarkVM.Marks = i.Marks;
                _objMarkVM.Student_id = i.Student_id;
                _objMarkVM.CreatedBy = i.CreatedBy;
                _objMarkVM.UpdatedBy = i.UpdatedBy;
                _objMarkVM.CreatedDate = i.CreatedDate;
                _objMarkVM.UpdatedDate = i.UpdatedDate;
                _objMarkVMList.Add(_objMarkVM);

            }


            return _objMarkVMList;
        }
    }
}
