﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolManagement.Models;

namespace SchoolManagement.ViewModel
{
    public class UserDetailVM
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string Designation { get; set; }
        public string Address { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public List<UserDetailVM> UserList { get; set; }


        public UserDetail ToModel(UserDetailVM _objUserVM)
        {
            UserDetail _objUser = new UserDetail();

            _objUser.ID = _objUserVM.ID;
            _objUser.Name = _objUserVM.Name;
            _objUser.Gender = _objUserVM.Gender;
            _objUser.Designation = _objUserVM.Designation;
            _objUser.Address = _objUserVM.Address;
            _objUser.MobileNo = _objUserVM.MobileNo;
            _objUser.Email = _objUserVM.Email;
            _objUser.Password = _objUserVM.Password;
            _objUser.CreatedBy = _objUserVM.CreatedBy;
            _objUser.UpdatedBy = _objUserVM.UpdatedBy;
            _objUser.CreatedDate = _objUserVM.CreatedDate;
            _objUser.UpdatedDate = _objUserVM.UpdatedDate;

            return _objUser;
        }

        public UserDetailVM ToViewModel(UserDetail _objUser)
        {
            UserDetailVM _objUserVM = new UserDetailVM();

            _objUserVM.ID = _objUser.ID;
            _objUserVM.Name = _objUser.Name;
            _objUserVM.Gender = _objUser.Gender;
            _objUserVM.Designation = _objUser.Designation;
            _objUserVM.Address = _objUser.Address;
            _objUserVM.MobileNo = _objUser.MobileNo;
            _objUserVM.Email = _objUser.Email;
            _objUserVM.Password = _objUser.Password;
            _objUserVM.CreatedBy = _objUser.CreatedBy;
            _objUserVM.UpdatedBy = _objUser.UpdatedBy;
            _objUserVM.CreatedDate = _objUser.CreatedDate;
            _objUserVM.UpdatedDate = _objUser.UpdatedDate;

            return _objUserVM;
        }
        public List<UserDetailVM> ToViewModelList(List<UserDetail> _objUserList)
        {
            
            List<UserDetailVM> _objUserVMList = new List<UserDetailVM>();

           foreach (var i in _objUserList)
            {

                    UserDetailVM _objUserVM = new UserDetailVM();
                    _objUserVM.ID = i.ID; 
                    _objUserVM.Name = i.Name;
                    _objUserVM.Gender = i.Gender;
                    _objUserVM.Designation = i.Designation;
                    _objUserVM.Address = i.Address;
                    _objUserVM.MobileNo = i.MobileNo;
                    _objUserVM.Email = i.Email;
                    _objUserVM.Password = i.Password;
                    _objUserVM.CreatedBy = i.CreatedBy;
                    _objUserVM.UpdatedBy = i.UpdatedBy;
                    _objUserVM.CreatedDate = i.CreatedDate;
                    _objUserVM.UpdatedDate = i.UpdatedDate; 
                     _objUserVMList.Add(_objUserVM);
                
            }
          

            return _objUserVMList;
        }

    }
}
