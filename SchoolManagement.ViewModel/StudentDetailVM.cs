﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolManagement.Models;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace SchoolManagement.ViewModel
{
    public class StudentDetailVM
    {
        public int ID { get; set; }
        public string Name { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DOB { get; set; }

        public string ImagePath { get; set; }

        [Display(Name = "Upload Photo")]
        public HttpPostedFileBase Image { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }

        [Display(Name = "Father's Name")]
        public string FatherName { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }

        [Display(Name = "Department ID")]
        public int Dept_Id { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public List<StudentDetailVM> StudentList { get; set; }

        public StudentDetail ToModel(StudentDetailVM _objStudentVM)
        {
            StudentDetail _objStudent = new StudentDetail();

            _objStudent.ID = _objStudentVM.ID;
            _objStudent.Name = _objStudentVM.Name;
            _objStudent.DOB = _objStudentVM.DOB;
            _objStudent.Image = _objStudentVM.ImagePath;
            _objStudent.Gender = _objStudentVM.Gender;
            _objStudent.Address = _objStudentVM.Address;
            _objStudent.FatherName = _objStudentVM.FatherName;
            _objStudent.MobileNo = _objStudentVM.MobileNo;
            _objStudent.Email = _objStudentVM.Email;
            _objStudent.Dept_Id = _objStudentVM.Dept_Id;
            _objStudent.CreatedBy = _objStudentVM.CreatedBy;
            _objStudent.UpdatedBy = _objStudentVM.UpdatedBy;
            _objStudent.CreatedDate = _objStudentVM.CreatedDate;
            _objStudent.UpdatedDate = _objStudentVM.UpdatedDate;

            return _objStudent;
        }

        public StudentDetailVM ToViewModel(StudentDetail _objStudent)
        {
            StudentDetailVM _objStudentVM = new StudentDetailVM();

            _objStudentVM.ID = _objStudent.ID;
            _objStudentVM.Name = _objStudent.Name;
            _objStudentVM.DOB = _objStudent.DOB;
            _objStudentVM.ImagePath = _objStudent.Image;
            
            _objStudentVM.Gender = _objStudent.Gender;
            _objStudentVM.Address = _objStudent.Address;
            _objStudentVM.FatherName = _objStudent.FatherName;
            _objStudentVM.MobileNo = _objStudent.MobileNo;
            _objStudentVM.Email = _objStudent.Email;
            _objStudentVM.Dept_Id = _objStudent.Dept_Id;
            _objStudentVM.CreatedBy = _objStudent.CreatedBy;
            _objStudentVM.UpdatedBy = _objStudent.UpdatedBy;
            _objStudentVM.CreatedDate = _objStudent.CreatedDate;
            _objStudentVM.UpdatedDate = _objStudent.UpdatedDate;

            return _objStudentVM;
        }
        public List<StudentDetailVM> ToViewModelList(List<StudentDetail> _objStudentList)
        {
         
            List<StudentDetailVM> _objStudentVMList = new List<StudentDetailVM>();

            foreach (var i in _objStudentList)
            {

                StudentDetailVM _objStudentVM = new StudentDetailVM();
                _objStudentVM.ID = i.ID;
                _objStudentVM.Name = i.Name;
                _objStudentVM.DOB = i.DOB;
                _objStudentVM.ImagePath = i.Image;
                
                _objStudentVM.Gender = i.Gender;
                _objStudentVM.Address = i.Address;
                _objStudentVM.FatherName = i.FatherName;
                _objStudentVM.MobileNo = i.MobileNo;
                _objStudentVM.Email = i.Email;
                _objStudentVM.Dept_Id = i.Dept_Id;
                _objStudentVM.CreatedBy = i.CreatedBy;
                _objStudentVM.UpdatedBy = i.UpdatedBy;
                _objStudentVM.CreatedDate = i.CreatedDate;
                _objStudentVM.UpdatedDate = i.UpdatedDate;
                _objStudentVMList.Add(_objStudentVM);

            }


            return _objStudentVMList;
        }

    }
}
